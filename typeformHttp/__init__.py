from flask import Flask

def create_app():
    # Create the application with the main methods
    app = Flask(__name__) # Use the package name from import

    @app.route('/')
    def status():
        return 'Server running'

    from . import echo
    app.register_blueprint(echo.bp)
    
    return app
