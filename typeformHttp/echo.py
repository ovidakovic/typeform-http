from flask import Blueprint, render_template

bp = Blueprint('echo', __name__, url_prefix='/')

@bp.route('/<path:inputText>')
def echo_text(inputText):
    return render_template('echo.html', display=inputText)