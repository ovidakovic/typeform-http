#!/bin/bash

# Install needed packages
pip install --user . coverage pytest

coverage run -m pytest
