import pytest

@pytest.mark.parametrize('path',(
    'foo',
    '/foo',
    '/foo/bar',
    '/foo/bar/'
))
def test_echo(client, path):
    response = client.get(path)
    assert b'foo' in response.data

@pytest.mark.parametrize('path',(
    'foo',
    '/foo',
    '/foo/bar',
    '/foo/bar/'
))
def test_echo_invalid_method(client, path):    
    response = client.post(path)
    assert response.status_code == 405
    response = client.put(path)
    assert response.status_code == 405
    response = client.delete(path)
    assert response.status_code == 405
    response = client.trace(path)
    assert response.status_code == 405
