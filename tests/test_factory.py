from typeformHttp import create_app

def test_running(client):
    response = client.get('/')
    assert response.data == b'Server running'
    